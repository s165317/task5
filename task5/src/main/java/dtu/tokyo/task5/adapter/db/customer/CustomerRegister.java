package dtu.tokyo.task5.adapter.db.customer;

import dtu.tokyo.task5.logic.*;

public interface CustomerRegister {
	public void storeCustomer(Customer customer);
	public boolean contianCustomer(String customerID);	
}
