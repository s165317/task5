package dtu.tokyo.task5.adapter.db.customer;

import java.util.HashMap;
import java.util.Vector;

import dtu.tokyo.task5.logic.Customer;
import dtu.tokyo.task5.logic.Token;

public class CustomerCollection implements CustomerRegister{
	HashMap<String, String> customerNames;
	HashMap<String, Vector<Token>> customerTokens;

	public CustomerCollection() {
		customerNames = new HashMap<>();
		customerTokens = new HashMap<>();
	}

	@Override
	public void storeCustomer(Customer c) {
		customerNames.put(c.getCustomerID(), c.getName());
		customerTokens.put(c.getCustomerID(),c.getTokens());
	}

	@Override
	public boolean contianCustomer(String customerID) {
		return customerNames.containsKey(customerID);
	}

}
