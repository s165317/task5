package dtu.tokyo.task5.adapter.db.token;

public interface TokenRegister {
	public void storeToken(String id);
	public boolean getStatusOfToken(String tokenId);
	public boolean containsToken(String tokenID);
	public void setStatusOfToken(String tokenID, boolean b);
}
